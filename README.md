# **run file with cuda**

##### **file: emotion_detection/visualize.py**

at line 24, delete the argument:
```map_location=torch.device('cpu')```

turn off comment at the 44 and 50 lines

##### **file: face_detection/one_image_run.py**

at line 24, edit:
`default=False`

# setting the environment

create a virtual environment:
`virtualenv -p python3.6 venv`

activate:
`source venv\bin\activate`

deactivate:
`deactivate`

install the libraries:
`pip install -r requirements.txt`

# create or reset the videos folder

mark the file as executable:
`chmod +x clean-video.run`

execute the file:
`./clean-video.run`

# run program

you should run this program by the debug tool (something errors about root path are found when I run it by terminal)

run the debug tool with script:
`python run.py`

if you want to change some parameters, edit it in `args.py` file
