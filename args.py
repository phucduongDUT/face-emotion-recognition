# number of the classes
num_classes = 7
# labels of the classes
classes = ['angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral']

# the path of video
video_path = './videos/test.mp4'
# number of second in a frames
num_secs_in_frames = 3
