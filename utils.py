from face_detection.one_image_run import prepare, predict_image
from emotion_detection.visualize import get_emotion

from collections import Counter
import cv2

import args

resize, device, _t, net, cfg, fw = prepare()


class VideoIterator:
    def __init__(self, path):
        self.video_capture = cv2.VideoCapture(path)
        self.frames = []
        self.fps = int(self.video_capture.get(cv2.CAP_PROP_FPS))
        self.num_secs_in_frames = 3

    def __iter__(self):
        while self.video_capture.isOpened():
            ret, frame = self.video_capture.read()
            self.frames.append(frame)

            if not ret:
                yield self.frames
                break

            if len(self.frames) == self.fps * self.num_secs_in_frames:
                yield self.frames
                self.frames = []

    def get_frame_width(self):
        return int(self.video_capture.get(3))

    def get_frame_height(self):
        return int(self.video_capture.get(4))


class VideoProcessor:
    def __init__(self, frame_width, frame_height):
        self.frame_width = frame_width
        self.frame_height = frame_height
        self.idx_frame = 0
        self.data = [
            {'writer': None, 'is_open': False, 'id_next_file': 0},
            {'writer': None, 'is_open': False, 'id_next_file': 0},
            {'writer': None, 'is_open': False, 'id_next_file': 0},
            {'writer': None, 'is_open': False, 'id_next_file': 0},
            {'writer': None, 'is_open': False, 'id_next_file': 0},
            {'writer': None, 'is_open': False, 'id_next_file': 0},
            {'writer': None, 'is_open': False, 'id_next_file': 0}
        ]

    def __update_writer(self, idx_emotion):
        idx_file = self.data[idx_emotion].get('id_next_file')
        name_file = '0' * (10 - len(str(idx_file))) + str(idx_file) + '.avi'
        path_file = './videos/' + args.classes[idx_emotion] + '/' + name_file

        self.data[idx_emotion]['is_open'] = True
        self.data[idx_emotion]['writer'] = cv2.VideoWriter(path_file,
                                                           cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'),
                                                           10, (self.frame_width, self.frame_height))
        self.data[idx_emotion]['id_next_file'] += 1

    def __add_frames_to_emotion(self, frames, idx_emotion):
        if not self.data[idx_emotion]['is_open']:
            self.__update_writer(idx_emotion=idx_emotion)

        for frame in frames:
            self.data[idx_emotion]['writer'].write(frame)

    def save_frames(self, frames, extracted_idx_emotions, faces):
        for idx_emotion in range(args.num_classes):
            if idx_emotion in extracted_idx_emotions:
                self.__add_frames_to_emotion(frames=frames, idx_emotion=idx_emotion)
            else:
                self.data[idx_emotion]['is_open'] = False


def bounding_box(image, data):
    if not (data.get('positions') and data.get('emotion')):
        return image
    width, height = image.shape[0]-1, image.shape[1]-1

    top = width if data.get('positions')[0] > width else data.get('positions')[0]
    bottom = width if data.get('positions')[1] > width else data.get('positions')[1]
    left = height if data.get('positions')[2] > height else data.get('positions')[2]
    right = height if data.get('positions')[3] > height else data.get('positions')[3]
    emotion = data.get('emotion')

    image[top, left:right] = 0
    image[bottom, left:right] = 0
    image[top:bottom, left] = 0
    image[top:bottom, right] = 0

    image = cv2.putText(image, emotion, (left, top), cv2.FONT_HERSHEY_SIMPLEX, 1, 0)
    # cv2.imshow('image', image)
    # cv2.waitKey(3)
    return image


def get_emotions_from_frames(frames):
    emotions, data = [], []
    for frame in frames:
        faces = predict_image(frame, resize, device, _t, net, cfg, fw)
        data.append([])
        for idx_face, face in enumerate(faces):
            idx_emotion = get_emotion(face.get('image'))
            data[-1].append({
                'emotion': args.classes[idx_emotion],
                'positions': face.get('positions')
            })
            emotions += [idx_emotion]
    emotions = dict(Counter(emotions))
    return [key for key in emotions.keys() if emotions.get(key) >= 5], data
