import numpy as np
import utils

import args


video_iterator = utils.VideoIterator(args.video_path)
frame_width = video_iterator.get_frame_width()
frame_height = video_iterator.get_frame_height()
video_processor = utils.VideoProcessor(frame_width=frame_width, frame_height=frame_height)

for frames in video_iterator:
    subframe = frames[::5]
    emotions, data = utils.get_emotions_from_frames(subframe)
    for idx_subframe in range(len(subframe)):
        for face in data[idx_subframe]:
            subframe[idx_subframe] = utils.bounding_box(subframe[idx_subframe], face)
    video_processor.save_frames(subframe, emotions, data)
